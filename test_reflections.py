import numpy as np
from numpy.testing import assert_allclose

import ctypes as ct
import numpy.ctypeslib as npct

C_lib = npct.load_library("./reflections", ".")

def reflections(depth, e_zH, Gam, lrec, lsrc, Rp, Rm, **kwargs):
    # print(kwargs)
    c_doublep = ct.POINTER(ct.c_double)
    nfreq, noff, nlayer, nlambda = Gam.shape
    C_lib.reflections(
        nfreq, noff, nlayer, nlambda,
        depth.ctypes.data_as(c_doublep), 
        e_zH.ctypes.data_as(c_doublep), 
        Gam.ctypes.data_as(c_doublep), 
        int(lrec),
        int(lsrc), 
        Rp.ctypes.data_as(c_doublep), 
        Rm.ctypes.data_as(c_doublep)
    )


# Data generated with create_data/kernel.py
DATAKERNEL = np.load('/home/dtr/Codes/empymod/tests/data/kernel.npz', allow_pickle=True)


dat = DATAKERNEL['reflections'][()]
for _, val in dat.items():
    for i in [2, 4, 6, 8, 10]:
        ab = val[0]
        TM = val[1]
        Rp = np.zeros_like(val[i+1][0])
        Rm = np.zeros_like(val[i+1][0])
        reflections(Rp=Rp, Rm=Rm, **val[i])
        assert_allclose(Rm, val[i+1][0])
        assert_allclose(Rp, val[i+1][1])
