import numpy as np
from numpy.testing import assert_allclose

import ctypes as ct
import numpy.ctypeslib as npct

C_lib = npct.load_library("./fields", ".")


def fields(depth, Rp, Rm, Gam, lrec, lsrc, zsrc, ab, TM, **kwargs):
    # print(kwargs)
    c_doublep = ct.POINTER(ct.c_double)
    nfreq, noff, nlayer, nlambda = Gam.shape
    C_lib.fields(
        nfreq, noff, nlayer, nlambda,
        depth.ctypes.data_as(c_doublep), Rp.ctypes.data_as(c_doublep),
        Rm.ctypes.data_as(c_doublep), Gam.ctypes.data_as(c_doublep), int(lrec),
        int(lsrc), ct.c_double(zsrc), int(ab), int(TM),
        Pu.ctypes.data_as(c_doublep), Pd.ctypes.data_as(c_doublep)
    )


# Data generated with create_data/kernel.py
DATAKERNEL = np.load('/home/dtr/Codes/empymod/tests/data/kernel.npz', allow_pickle=True)


dat = DATAKERNEL['fields'][()]
for _, val in dat.items():
    for i in [2, 4, 6, 8, 10]:
        ab = val[0]
        TM = val[1]
        Pd = np.zeros_like(val[i+1][0])
        Pu = np.zeros_like(val[i+1][0])
        fields(ab=ab, TM=TM, Pd=Pd, Pu=Pu, **val[i])
        assert_allclose(Pu, val[i+1][0])
        assert_allclose(Pd, val[i+1][1])
