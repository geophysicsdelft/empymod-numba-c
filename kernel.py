# Copyright 2016 The emsig community.
#
# This file is part of empymod.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy
# of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.


import numpy as np
import numba as nb

_numba_setting = {'nogil': True, 'cache': True}
_numba_with_fm = {'fastmath': True, **_numba_setting}


@nb.njit(**_numba_setting)
def wavenumber(zsrc, zrec, lsrc, lrec, depth, etaH, etaV, zetaH, zetaV, lambd,
               ab, xdirect, msrc, mrec):
    """Calculate wavenumber domain solution."""
    nfreq, _ = etaH.shape
    noff, nlambda = lambd.shape

    # ** CALCULATE GREEN'S FUNCTIONS
    # Shape of PTM, PTE: (nfreq, noffs, nfilt)
    PTM, PTE = greenfct(zsrc, zrec, lsrc, lrec, depth, etaH, etaV, zetaH,
                        zetaV, lambd, ab, xdirect, msrc, mrec)

    # ** AB-SPECIFIC COLLECTION OF PJ0, PJ1, AND PJ0b

    # Pre-allocate output
    if ab in [11, 22, 24, 15, 33]:
        PJ0 = np.zeros_like(PTM)
    else:
        PJ0 = None
    if ab in [11, 12, 21, 22, 14, 24, 15, 25]:
        PJ0b = np.zeros_like(PTM)
    else:
        PJ0b = None
    if ab not in [33, ]:
        PJ1 = np.zeros_like(PTM)
    else:
        PJ1 = None
    Ptot = np.zeros_like(PTM)

    # Calculate Ptot which is used in all cases
    fourpi = 4*np.pi
    for i in range(nfreq):
        for ii in range(noff):
            for iv in range(nlambda):
                Ptot[i, ii, iv] = (PTM[i, ii, iv] + PTE[i, ii, iv])/fourpi

    # If rec is magnetic switch sign (reciprocity MM/ME => EE/EM).
    if mrec:
        sign = -1
    else:
        sign = 1

    # Group into PJ0 and PJ1 for J0/J1 Hankel Transform
    if ab in [11, 12, 21, 22, 14, 24, 15, 25]:    # Eqs 105, 106, 111, 112,
        # J2(kr) = 2/(kr)*J1(kr) - J0(kr)         #     119, 120, 123, 124
        if ab in [14, 22]:
            sign *= -1

        for i in range(nfreq):
            for ii in range(noff):
                for iv in range(nlambda):
                    PJ0b[i, ii, iv] = sign/2*Ptot[i, ii, iv]*lambd[ii, iv]
                    PJ1[i, ii, iv] = -sign*Ptot[i, ii, iv]

        if ab in [11, 22, 24, 15]:
            if ab in [22, 24]:
                sign *= -1

            eightpi = sign*8*np.pi
            for i in range(nfreq):
                for ii in range(noff):
                    for iv in range(nlambda):
                        PJ0[i, ii, iv] = PTM[i, ii, iv] - PTE[i, ii, iv]
                        PJ0[i, ii, iv] *= lambd[ii, iv]/eightpi

    elif ab in [13, 23, 31, 32, 34, 35, 16, 26]:  # Eqs 107, 113, 114, 115,
        if ab in [34, 26]:                        # .   121, 125, 126, 127
            sign *= -1
        for i in range(nfreq):
            for ii in range(noff):
                for iv in range(nlambda):
                    dlambd = lambd[ii, iv]*lambd[ii, iv]
                    PJ1[i, ii, iv] = sign*Ptot[i, ii, iv]*dlambd

    elif ab in [33, ]:                            # Eq 116
        for i in range(nfreq):
            for ii in range(noff):
                for iv in range(nlambda):
                    tlambd = lambd[ii, iv]*lambd[ii, iv]*lambd[ii, iv]
                    PJ0[i, ii, iv] = sign*Ptot[i, ii, iv]*tlambd

    # Return PJ0, PJ1, PJ0b
    return PJ0, PJ1, PJ0b


@nb.njit(**_numba_setting)
def greenfct(zsrc, zrec, lsrc, lrec, depth, etaH, etaV, zetaH, zetaV, lambd,
             ab, xdirect, msrc, mrec):
    r"""Calculate Green's function for TM and TE."""
    nfreq, nlayer = etaH.shape
    noff, nlambda = lambd.shape

    # GTM/GTE have shape (frequency, offset, lambda).
    # gamTM/gamTE have shape (frequency, offset, layer, lambda):

    # Reciprocity switches for magnetic receivers
    if mrec:
        if msrc:  # If src is also magnetic, switch eta and zeta (MM => EE).
            # G^mm_ab(s, r, e, z) = -G^ee_ab(s, r, -z, -e)
            etaH, zetaH = -zetaH, -etaH
            etaV, zetaV = -zetaV, -etaV
        else:  # If src is electric, swap src and rec (ME => EM).
            # G^me_ab(s, r, e, z) = -G^em_ba(r, s, e, z)
            zsrc, zrec = zrec, zsrc
            lsrc, lrec = lrec, lsrc

    for TM in [True, False]:

        # Continue if Green's function not required
        if TM and ab in [16, 26]:
            continue
        elif not TM and ab in [13, 23, 31, 32, 33, 34, 35]:
            continue

        # Define eta/zeta depending if TM or TE
        if TM:
            e_zH, e_zV, z_eH = etaH, etaV, zetaH   # TM: zetaV not used
        else:
            e_zH, e_zV, z_eH = zetaH, zetaV, etaH  # TE: etaV not used

        # Uppercase gamma
        Gam = np.zeros((nfreq, noff, nlayer, nlambda), etaH.dtype)
        for i in range(nfreq):
            for ii in range(noff):
                for iii in range(nlayer):
                    h_div_v = e_zH[i, iii]/e_zV[i, iii]
                    h_times_h = z_eH[i, iii]*e_zH[i, iii]
                    for iv in range(nlambda):
                        l2 = lambd[ii, iv]*lambd[ii, iv]
                        Gam[i, ii, iii, iv] = np.sqrt(h_div_v*l2 + h_times_h)

        # Gamma in receiver layer
        lrecGam = Gam[:, :, lrec, :]

        # Reflection (coming from below (Rp) and above (Rm) rec)
        if depth.size > 1:  # Only if more than 1 layer
            Rp, Rm = reflections(depth, e_zH, Gam, lrec, lsrc)

            # Field propagators
            # (Up- (Wu) and downgoing (Wd), in rec layer); Eq 74
            Wu = np.zeros_like(lrecGam)
            Wd = np.zeros_like(lrecGam)

            if lrec != depth.size-1:  # No upgoing field prop. if rec in last
                ddepth = depth[lrec + 1] - zrec
                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):
                            Wu[i, ii, iv] = np.exp(-lrecGam[i, ii, iv]*ddepth)

            if lrec != 0:     # No downgoing field propagator if rec in first
                ddepth = zrec - depth[lrec]
                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):
                            Wd[i, ii, iv] = np.exp(-lrecGam[i, ii, iv]*ddepth)

            # Field at rec level (coming from below (Pu) and above (Pd) rec)
            Pu, Pd = fields(depth, Rp, Rm, Gam, lrec, lsrc, zsrc, ab, TM)

        # Green's functions
        green = np.zeros_like(lrecGam)
        if lsrc == lrec:  # Rec in src layer; Eqs 108, 109, 110, 117, 118, 122

            # Green's function depending on <ab>
            # (If only one layer, no reflections/fields)
            if depth.size > 1 and ab in [13, 23, 31, 32, 14, 24, 15, 25]:
                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):
                            green[i, ii, iv] = Pu[i, ii, iv]*Wu[i, ii, iv]
                            green[i, ii, iv] -= Pd[i, ii, iv]*Wd[i, ii, iv]

            elif depth.size > 1:
                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):
                            green[i, ii, iv] = Pu[i, ii, iv]*Wu[i, ii, iv]
                            green[i, ii, iv] += Pd[i, ii, iv]*Wd[i, ii, iv]

            # Direct field, if it is computed in the wavenumber domain
            if not xdirect:
                ddepth = abs(zsrc - zrec)
                dsign = np.sign(zrec - zsrc)
                minus_ab = [11, 12, 13, 14, 15, 21, 22, 23, 24, 25]

                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):

                            # Direct field
                            directf = np.exp(-lrecGam[i, ii, iv]*ddepth)

                            # Swap TM for certain <ab>
                            if TM and ab in minus_ab:
                                directf *= -1

                            # Multiply by zrec-zsrc-sign for certain <ab>
                            if ab in [13, 14, 15, 23, 24, 25, 31, 32]:
                                directf *= dsign

                            # Add direct field to Green's function
                            green[i, ii, iv] += directf

        else:

            # Calculate exponential factor
            if lrec == depth.size-1:
                ddepth = 0
            else:
                ddepth = depth[lrec+1] - depth[lrec]

            fexp = np.zeros_like(lrecGam)
            for i in range(nfreq):
                for ii in range(noff):
                    for iv in range(nlambda):
                        fexp[i, ii, iv] = np.exp(-lrecGam[i, ii, iv]*ddepth)

            # Sign-switch for Green calculation
            if TM and ab in [11, 12, 13, 21, 22, 23, 14, 24, 15, 25]:
                pmw = -1
            else:
                pmw = 1

            if lrec < lsrc:  # Rec above src layer: Pd not used
                #              Eqs 89-94, A18-A23, B13-B15
                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):
                            green[i, ii, iv] = Pu[i, ii, iv]*(
                                    Wu[i, ii, iv] + pmw*Rm[i, ii, 0, iv] *
                                    fexp[i, ii, iv]*Wd[i, ii, iv])

            elif lrec > lsrc:  # rec below src layer: Pu not used
                #                Eqs 97-102 A26-A30, B16-B18
                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):
                            green[i, ii, iv] = Pd[i, ii, iv]*(
                                    pmw*Wd[i, ii, iv] +
                                    Rp[i, ii, abs(lsrc-lrec), iv] *
                                    fexp[i, ii, iv]*Wu[i, ii, iv])

        # Store in corresponding variable
        if TM:
            gamTM, GTM = Gam, green
        else:
            gamTE, GTE = Gam, green

    # ** AB-SPECIFIC FACTORS AND CALCULATION OF PTOT'S
    # These are the factors inside the integrals
    # Eqs 105-107, 111-116, 119-121, 123-128

    if ab in [11, 12, 21, 22]:
        for i in range(nfreq):
            for ii in range(noff):
                for iv in range(nlambda):
                    GTM[i, ii, iv] *= gamTM[i, ii, lrec, iv]/etaH[i, lrec]
                    GTE[i, ii, iv] *= zetaH[i, lsrc]/gamTE[i, ii, lsrc, iv]

    elif ab in [14, 15, 24, 25]:
        for i in range(nfreq):
            fact = etaH[i, lsrc]/etaH[i, lrec]
            for ii in range(noff):
                for iv in range(nlambda):
                    GTM[i, ii, iv] *= fact*gamTM[i, ii, lrec, iv]
                    GTM[i, ii, iv] /= gamTM[i, ii, lsrc, iv]

    elif ab in [13, 23]:
        GTE = np.zeros_like(GTM)
        for i in range(nfreq):
            fact = etaH[i, lsrc]/etaH[i, lrec]/etaV[i, lsrc]
            for ii in range(noff):
                for iv in range(nlambda):
                    GTM[i, ii, iv] *= -fact*gamTM[i, ii, lrec, iv]
                    GTM[i, ii, iv] /= gamTM[i, ii, lsrc, iv]

    elif ab in [31, 32]:
        GTE = np.zeros_like(GTM)
        for i in range(nfreq):
            for ii in range(noff):
                for iv in range(nlambda):
                    GTM[i, ii, iv] /= etaV[i, lrec]

    elif ab in [34, 35]:
        GTE = np.zeros_like(GTM)
        for i in range(nfreq):
            fact = etaH[i, lsrc]/etaV[i, lrec]
            for ii in range(noff):
                for iv in range(nlambda):
                    GTM[i, ii, iv] *= fact/gamTM[i, ii, lsrc, iv]

    elif ab in [16, 26]:
        GTM = np.zeros_like(GTE)
        for i in range(nfreq):
            fact = zetaH[i, lsrc]/zetaV[i, lsrc]
            for ii in range(noff):
                for iv in range(nlambda):
                    GTE[i, ii, iv] *= fact/gamTE[i, ii, lsrc, iv]

    elif ab in [33, ]:
        GTE = np.zeros_like(GTM)
        for i in range(nfreq):
            fact = etaH[i, lsrc]/etaV[i, lsrc]/etaV[i, lrec]
            for ii in range(noff):
                for iv in range(nlambda):
                    GTM[i, ii, iv] *= fact/gamTM[i, ii, lsrc, iv]

    # Return Green's functions
    return GTM, GTE


@nb.njit(**_numba_with_fm)
def reflections(depth, e_zH, Gam, lrec, lsrc):
    r"""Calculate Rp, Rm."""

    # Get numbers and max/min layer.
    nfreq, noff, nlambda = Gam[:, :, 0, :].shape
    maxl = max([lrec, lsrc])
    minl = min([lrec, lsrc])

    # Loop over Rp, Rm
    for plus in [True, False]:

        # Switches depending if plus or minus
        if plus:
            pm = 1
            layer_count = np.arange(depth.size-2, minl-1, -1)
            izout = abs(lsrc-lrec)
            minmax = pm*maxl
        else:
            pm = -1
            layer_count = np.arange(1, maxl+1, 1)
            izout = 0
            minmax = pm*minl

        # If rec in last  and rec below src (plus) or
        # if rec in first and rec above src (minus), shift izout
        shiftplus = lrec < lsrc and lrec == 0 and not plus
        shiftminus = lrec > lsrc and lrec == depth.size-1 and plus
        if shiftplus or shiftminus:
            izout -= pm

        # Pre-allocate Ref and rloc
        Ref = np.zeros_like(Gam[:, :, :maxl-minl+1, :])
        rloc = np.zeros_like(Gam[:, :, 0, :])

        # Calculate the reflection
        for iz in layer_count:

            # Eqs 65, A-12
            for i in range(nfreq):
                ra = e_zH[i, iz+pm]
                rb = e_zH[i, iz]
                for ii in range(noff):
                    for iv in range(nlambda):
                        rloca = ra*Gam[i, ii, iz, iv]
                        rlocb = rb*Gam[i, ii, iz+pm, iv]
                        rloc[i, ii, iv] = (rloca - rlocb)/(rloca + rlocb)

            # In first layer tRef = rloc
            if iz == layer_count[0]:
                tRef = rloc.copy()
            else:
                ddepth = depth[iz+1+pm]-depth[iz+pm]

                # Eqs 64, A-11
                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):
                            term = tRef[i, ii, iv]*np.exp(
                                    -2*Gam[i, ii, iz+pm, iv]*ddepth)
                            tRef[i, ii, iv] = (rloc[i, ii, iv] + term)/(
                                    1 + rloc[i, ii, iv]*term)

            # The global reflection coefficient is given back for all layers
            # between and including src- and rec-layer
            if lrec != lsrc and pm*iz <= minmax:
                Ref[:, :, izout, :] = tRef[:]
                izout -= pm

        # If lsrc = lrec, we just store the last values
        if lsrc == lrec and layer_count.size > 0:
            out = np.zeros_like(Ref[:, :, :1, :])
            out[:, :, 0, :] = tRef
        else:
            out = Ref

        # Store Ref in Rm/Rp
        if plus:
            Rp = out
        else:
            Rm = out

    # Return reflections (minus and plus)
    return Rp, Rm


# @nb.njit(**_numba_with_fm, parallel=True)  ## nb.prange()
@nb.njit(**_numba_setting)
def fields(depth, Rp, Rm, Gam, lrec, lsrc, zsrc, ab, TM):
    r"""Calculate Pu+, Pu-, Pd+, Pd-."""

    nfreq, noff, nlambda = Gam[:, :, 0, :].shape

    # Variables
    nlsr = abs(lsrc-lrec)+1  # nr of layers btw and incl. src and rec layer
    rsrcl = 0  # src-layer in reflection (Rp/Rm), first if down
    izrange = range(2, nlsr)
    isr = lsrc
    last = depth.size-1

    # Booleans if src in first or last layer; swapped if up=True
    first_layer = lsrc == 0
    last_layer = lsrc == depth.size-1

    # Depths; dp and dm are swapped if up=True
    if lsrc != depth.size-1:
        ds = depth[lsrc+1]-depth[lsrc]
        dp = depth[lsrc+1]-zsrc
    dm = zsrc-depth[lsrc]

    # Rm and Rp; swapped if up=True
    Rmp = Rm
    Rpm = Rp

    # Boolean if plus or minus has to be calculated
    plusset = [13, 23, 33, 14, 24, 34, 15, 25, 35]
    if TM:
        plus = ab in plusset
    else:
        plus = ab not in plusset

    # Sign-switches
    pm = 1     # + if plus=True, - if plus=False
    if not plus:
        pm = -1
    pup = -1   # + if up=True,   - if up=False
    mupm = 1   # + except if up=True and plus=False

    # Gamma of source layer
    iGam = Gam[:, :, lsrc, :]

    # Calculate down- and up-going fields
    for up in [False, True]:

        # No upgoing field if rec is in last layer or below src
        if up and (lrec == depth.size-1 or lrec > lsrc):
            Pu = np.zeros_like(iGam)
            continue
        # No downgoing field if rec is in first layer or above src
        if not up and (lrec == 0 or lrec < lsrc):
            Pd = np.zeros_like(iGam)
            continue

        # Swaps if up=True
        if up:
            if not last_layer:
                dp, dm = dm, dp
            else:
                dp = dm
            Rmp, Rpm = Rpm, Rmp
            first_layer, last_layer = last_layer, first_layer
            rsrcl = nlsr-1  # src-layer in refl. (Rp/Rm), last (nlsr-1) if up
            izrange = range(nlsr-2)
            isr = lrec
            last = 0
            pup = 1
            if not plus:
                mupm = -1

        P = np.zeros_like(iGam)

        # Calculate Pu+, Pu-, Pd+, Pd-
        if lsrc == lrec:  # rec in src layer; Eqs  81/82, A-8/A-9
            if last_layer:  # If src/rec are in top (up) or bottom (down) layer
                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):
                            tRmp = Rmp[i, ii, 0, iv]
                            tiGam = iGam[i, ii, iv]
                            P[i, ii, iv] = tRmp*np.exp(-tiGam*dm)

            else:           # If src and rec are in any layer in between
                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):
                            tiGam = iGam[i, ii, iv]
                            tRpm = Rpm[i, ii, 0, iv]
                            tRmp = Rmp[i, ii, 0, iv]
                            p1 = np.exp(-tiGam*dm)
                            p2 = pm*tRpm*np.exp(-tiGam*(ds+dp))
                            p3 = 1 - tRmp * tRpm * np.exp(-2*tiGam*ds)
                            P[i, ii, iv] = (p1 + p2) * tRmp/p3

        else:           # rec above (up) / below (down) src layer
            #           # Eqs  95/96,  A-24/A-25 for rec above src layer
            #           # Eqs 103/104, A-32/A-33 for rec below src layer

            # First compute P_{s-1} (up) / P_{s+1} (down)
            iRpm = Rpm[:, :, rsrcl, :]
            if first_layer:  # If src is in bottom (up) / top (down) layer
                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):
                            tiRpm = iRpm[i, ii, iv]
                            tiGam = iGam[i, ii, iv]
                            P[i, ii, iv] = (1 + tiRpm)*mupm*np.exp(-tiGam*dp)
            else:
                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):
                            iRmp = Rmp[i, ii, rsrcl, iv]
                            tiGam = iGam[i, ii, iv]
                            tRpm = iRpm[i, ii, iv]
                            p1 = mupm*np.exp(-tiGam*dp)
                            p2 = pm*mupm*iRmp*np.exp(-tiGam * (ds+dm))
                            p3 = (1 + tRpm)/(1 - iRmp*tRpm*np.exp(-2*tiGam*ds))
                            P[i, ii, iv] = (p1 + p2) * p3

            # If up or down and src is in last but one layer
            if up or (not up and lsrc+1 < depth.size-1):
                ddepth = depth[lsrc+1-1*pup]-depth[lsrc-1*pup]
                for i in range(nfreq):
                    for ii in range(noff):
                        for iv in range(nlambda):
                            tiRpm = Rpm[i, ii, rsrcl-1*pup, iv]
                            tiGam = Gam[i, ii, lsrc-1*pup, iv]
                            P[i, ii, iv] /= 1 + tiRpm*np.exp(-2*tiGam*ddepth)

            # Second compute P for all other layers
            if nlsr > 2:
                for iz in izrange:
                    ddepth = depth[isr+iz+pup+1]-depth[isr+iz+pup]
                    for i in range(nfreq):
                        for ii in range(noff):
                            for iv in range(nlambda):
                                tiRpm = Rpm[i, ii, iz+pup, iv]
                                piGam = Gam[i, ii, isr+iz+pup, iv]
                                p1 = (1+tiRpm)*np.exp(-piGam*ddepth)
                                P[i, ii, iv] *= p1

                    # If rec/src NOT in first/last layer (up/down)
                    if isr+iz != last:
                        ddepth = depth[isr+iz+1] - depth[isr+iz]
                        for i in range(nfreq):
                            for ii in range(noff):
                                for iv in range(nlambda):
                                    tiRpm = Rpm[i, ii, iz, iv]
                                    piGam2 = Gam[i, ii, isr+iz, iv]
                                    p1 = 1 + tiRpm*np.exp(-2*piGam2 * ddepth)
                                    P[i, ii, iv] /= p1

        # Store P in Pu/Pd
        if up:
            Pu = P
        else:
            Pd = P

    # Return fields (up- and downgoing)
    return Pu, Pd
