import numpy as np

import emg3d

import ctypes as ct
import numpy.ctypeslib as npct

C_lib = npct.load_library("./reflections.so", ".")

def reflections(depth, e_zH, Gam, lrec, lsrc, Rp, Rm, **kwargs):
    # print(kwargs)
    c_doublep = ct.POINTER(ct.c_double)
    nfreq, noff, nlayer, nlambda = Gam.shape
    C_lib.reflections(
        int(nfreq), int(noff), int(nlayer), int(nlambda),
        depth.ctypes.data_as(c_doublep), 
        e_zH.ctypes.data_as(c_doublep), 
        Gam.ctypes.data_as(c_doublep), 
        int(lrec),
        int(lsrc), 
        Rp.ctypes.data_as(c_doublep), 
        Rm.ctypes.data_as(c_doublep)
    )

# testsize = 'small'
# testsize = 'medium'
testsize = 'big'

out = {}
for TM in [True]:
    # Get data
    input = emg3d.load(f'h5/reflections_input_{testsize}.h5', verb=0)
    result = emg3d.load(f'h5/reflections_output_{testsize}.h5', verb=0)

    # Extract result
    rRp = result['Rp']
    rRm = result['Rm']

    # Pre-allocate output
    Rp = np.zeros_like(rRp)
    Rm = np.zeros_like(rRm)

    print(Rp.shape)

    #print(input)
    # Call function
    reflections(**input, Rm=Rm, Rp=Rp)

    out[str(TM)] = {'rRp': rRp, 'rRm': rRm, 'Rp': Rp, 'Rm': Rm}

# Compare
print(80*'-')
print(rRp)
print(80*'-')
print(Rp)
print(80*'-')
print(f"Same (TM=True)  : Rp: {np.allclose(out['True']['Rp'], out['True']['rRp'], rtol=1e-7, atol=0)}, Rm: {np.allclose(out['True']['Rm'], out['True']['rRm'], rtol=1e-7, atol=0)}")
#print(f"Same (TM=False) : Rp: {np.allclose(out['False']['Rp'], out['False']['rRp']), rtol=1e-7, atol=0}, Rm: {np.allclose(out['False']['Rm'], out['False']['rRm']), rtol=1e-7, atol=0}")
print(f"{30*'='}  {testsize}  {30*'='}")
