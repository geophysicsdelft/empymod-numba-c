import numpy as np

import emg3d

import ctypes as ct
import numpy.ctypeslib as npct

C_lib = npct.load_library("./greenfct.so", ".")

def greenfct(zsrc, zrec, lsrc, lrec, depth, etaH, etaV, zetaH, zetaV, lambd,
             ab, xdirect, msrc, mrec, **kwargs):
    # print(kwargs)
    c_doublep = ct.POINTER(ct.c_double)
    nfreq, nlayer = etaH.shape
    noff, nlambda = lambd.shape
    C_lib.greenfct(
        int(nfreq), int(noff), int(nlayer), int(nlambda),
        ct.c_double(zsrc), ct.c_double(zrec), 
        int(lsrc), int(lrec), 
        depth.ctypes.data_as(c_doublep), 
        etaH.ctypes.data_as(c_doublep), 
        etaV.ctypes.data_as(c_doublep), 
        zetaH.ctypes.data_as(c_doublep), 
        zetaV.ctypes.data_as(c_doublep), 
        lambd.ctypes.data_as(c_doublep), 
        int(ab), int(xdirect), int(msrc), int(mrec),
        GTM.ctypes.data_as(c_doublep), 
        GTE.ctypes.data_as(c_doublep)
    )

# testsize = 'small'
# testsize = 'medium'
testsize = 'big'

# Get data
input = emg3d.load(f'h5/greenfct_input_{testsize}.h5', verb=0)
result = emg3d.load(f'h5/greenfct_output_{testsize}.h5', verb=0)

# Extract result
rGTM = result['GTM']
rGTE = result['GTE']

# Pre-allocate output
GTM = np.zeros_like(rGTM)
GTE = np.zeros_like(rGTE)

print(80*'-')
print(f"Shapes GTM, GTE : {GTM.shape}, {GTE.shape}")
print(80*'-')
print(' === INPUT ===')
print(input)

# Call function
greenfct(**input, GTE=GTE, GTM=GTM)

# Compare
print(80*'-')
print(rGTM)
print(80*'-')
print(GTM)
print(80*'-')
print(f"Same : GTM: {np.allclose(GTM, rGTM, rtol=1e-7, atol=0)}, GTE: {np.allclose(GTE, rGTE, rtol=1e-7, atol=0)}")
print(f"{30*'='}  {testsize}  {30*'='}")
