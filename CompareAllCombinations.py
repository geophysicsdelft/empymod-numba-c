"""

To run this script:

- Clone the empymod repo
- Checkout the branch `replace-numba-by-c`
- Run `make install` in the empymod-root
- Run the script


Loop over four cases:
- source in first, second, third, last layer
  - receiver in first, second, third, last layer
    - electric/magnetic source
      - electric/magnetic receiver

This tests all possibilities.
"""

import numpy as np
import empymod

hankel_dlf_n = empymod.transform.hankel_dlf
hankel_dlf_c = empymod.transform.hankel_cdlf


depths = [-10, 5, 15, 25]

inp = {
    'depth': [0, 10, 20],
    'res': [1, 10, 5, 20],
    'freqtime': np.logspace(-3, 3, 11),
    'verb': 1,
}

atol, rtol = 0, 1e-12
print(f"\n{80*'='}")
print(f"Testing parameters: atol={atol}; rtol={rtol}\n")

for lsrc in range(len(depths)):
    inp['src'] = [0, 0, depths[lsrc]-1, 10, 40]

    for lrec in range(len(depths)):

        # JAN - Inline (y=0) works for most cases; but otherwise not
        # inp['rec'] = [100, 0, depths[lrec]+1, 30, 20]  # Inline works for many
        inp['rec'] = [80, 40, depths[lrec]+1, 30, 20]    # Most fail

        print(f"** lsrc: {lsrc}, lrec: {lrec} **")

        for msrc in [False, True]:
            inp['msrc'] = msrc

            for mrec in [False, True]:
                inp['mrec'] = mrec

                # Numba version
                empymod.transform.hankel_dlf = hankel_dlf_n
                nresult = empymod.bipole(**inp)

                # C version
                empymod.transform.hankel_dlf = hankel_dlf_c
                cresult = empymod.bipole(**inp)

                # Compare
                same = np.allclose(nresult, cresult, rtol=rtol, atol=atol,
                                   equal_nan=True)
                error = np.abs(nresult - cresult)
                if not same:
                    print(f"  $$ msrc: {msrc}; mrec: {mrec} -- ", end="")
                    print(f"{['❌', '✅'][same]}")
                    print(f"     => numba ***")
                    print(nresult)
                    print(f"     =>   C   ***")
                    print(cresult)
                    print()
                    print(f"    Data {['are NOT', 'ARE'][same]} the same "
                        f"(given rtol={rtol}, atol={atol})\n"
                        f"    max abs error: {np.max(error):.4g}; max rel "
                        f"error: {np.max(error/np.abs(cresult)):.4g}\n")

        if not (lsrc == len(depths)-1) or not (lrec == len(depths)-1):
            print(80*'-', end="\n")

print(80*'=', end='\n\n')
