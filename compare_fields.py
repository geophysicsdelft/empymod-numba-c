import numpy as np

import emg3d

import ctypes as ct
import numpy.ctypeslib as npct

C_lib = npct.load_library("./fields.so", ".")


def fields(nfreq, noff, nlayer, nlambda, depth, Rp, Rm, Gam,
           lrec, lsrc, zsrc, ab, TM, Pu, Pd, **kwargs):
    # print(kwargs)
    c_doublep = ct.POINTER(ct.c_double)
    C_lib.fields(
        int(nfreq), int(noff), int(nlayer), int(nlambda),
        depth.ctypes.data_as(c_doublep), Rp.ctypes.data_as(c_doublep),
        Rm.ctypes.data_as(c_doublep), Gam.ctypes.data_as(c_doublep), int(lrec),
        int(lsrc), ct.c_double(zsrc), int(ab), int(TM),
        Pu.ctypes.data_as(c_doublep), Pd.ctypes.data_as(c_doublep)
    )

# testsize = 'small'
# testsize = 'medium'
testsize = 'big'

out = {}
for TM in [True, False]:
    # Get data
    input = emg3d.load(f'h5/fields_input_{TM}_{testsize}.h5', verb=0)
    result = emg3d.load(f'h5/fields_output_{TM}_{testsize}.h5', verb=0)

    # Extract result
    rPd = result['Pd']
    rPu = result['Pu']

    # Pre-allocate output
    Pd = np.zeros_like(rPd)
    Pu = np.zeros_like(rPu)

    # Call function
    fields(**input, Pu=Pu, Pd=Pd)

    out[str(TM)] = {'rPd': rPd, 'rPu': rPu, 'Pd': Pd, 'Pu': Pu}

# Compare
# print(80*'-')
# print(rPu)
# print(80*'-')
# print(Pu)
print(80*'-')
print(f"Same (TM=True)  : Pu: {np.allclose(out['True']['Pu'], out['True']['rPu'], rtol=1e-7, atol=0)}, Pd: {np.allclose(out['True']['Pd'], out['True']['rPd'], rtol=1e-7, atol=0)}")
print(f"Same (TM=False) : Pu: {np.allclose(out['False']['Pu'], out['False']['rPu'], rtol=1e-7, atol=0)}, Pd: {np.allclose(out['False']['Pd'], out['False']['rPd'], rtol=1e-7, atol=0)}")
print(f"{30*'='}  {testsize}  {30*'='}")
