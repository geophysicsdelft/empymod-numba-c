import numpy as np

import emg3d

import ctypes as ct
import numpy.ctypeslib as npct

C_lib = npct.load_library("./wavenumber.so", ".")

def wavenumber(zsrc, zrec, lsrc, lrec, depth, etaH, etaV, zetaH, zetaV, lambd,
             ab, xdirect, msrc, mrec, PJ0, PJ1, PJ0b, **kwargs):
    # print(kwargs)
    c_doublep = ct.POINTER(ct.c_double)
    nfreq, nlayer = etaH.shape
    noff, nlambda = lambd.shape
    C_lib.wavenumber(
        int(nfreq), int(noff), int(nlayer), int(nlambda),
        ct.c_double(zsrc), ct.c_double(zrec), 
        int(lsrc), int(lrec), 
        depth.ctypes.data_as(c_doublep), 
        etaH.ctypes.data_as(c_doublep), 
        etaV.ctypes.data_as(c_doublep), 
        zetaH.ctypes.data_as(c_doublep), 
        zetaV.ctypes.data_as(c_doublep), 
        lambd.ctypes.data_as(c_doublep), 
        int(ab), int(xdirect), int(msrc), int(mrec),
        PJ0.ctypes.data_as(c_doublep), 
        PJ1.ctypes.data_as(c_doublep),
        PJ0b.ctypes.data_as(c_doublep)
    )

testsize = 'tiny'
# testsize = 'small'
# testsize = 'medium'
# testsize = 'big'

# Get data
input = emg3d.load(f'h5/wavenumber_input_{testsize}.h5', verb=0)
result = emg3d.load(f'h5/wavenumber_output_{testsize}.h5', verb=0)
print(result.keys())

# Extract result
rPJ0 = result['PJ0']
rPJ1 = result['PJ1']
rPJ0b = result['PJ0b']

# Pre-allocate output
PJ0 = np.zeros_like(rPJ0)
PJ1 = np.zeros_like(rPJ1)
PJ0b = np.zeros_like(rPJ0b)

print(80*'-')
print(f"Shapes PJ0, PJ1, PJ0b: {PJ0.shape}, {PJ1.shape}, {PJ0b.shape}")
print(80*'-')
print(' === INPUT ===')
print(input)

# Call function
wavenumber(**input, PJ0=PJ0, PJ1=PJ1, PJ0b=PJ0b)

# Compare
print(80*'-')
print(rPJ1)
print(80*'-')
print(PJ1)
print(80*'-')
print(f"Same : PJ0: {np.allclose(PJ0, rPJ0, rtol=1e-7, atol=0)}, PJ1: {np.allclose(PJ1, rPJ1, rtol=1e-7, atol=0)}, PJ0b: {np.allclose(PJ0b, rPJ0b, rtol=1e-7, atol=0)}")
print(f"{30*'='}  {testsize}  {30*'='}")
